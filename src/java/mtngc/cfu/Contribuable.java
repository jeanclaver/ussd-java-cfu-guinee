/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu;

/**
 *
 * @author Administrateur
 */
public class Contribuable {
    
    private String idguincode;
    private String nom;
    private String prenom;
    private String commune;
    private String  quartier;
    private int  montantcfu;
    private int impaye;
    private int solde;
    
   
    public String getIdguincode() {
        return idguincode;
    }

    public void setId_guincode(String id_guincode) {
        this.idguincode = id_guincode;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public int getMontantcfu() {
        return montantcfu;
    }

    public void setMontantcfu(int montantcfu) {
        this.montantcfu = montantcfu;
    }

    public int getImpaye() {
        return impaye;
    }

    public void setImpaye(int impaye) {
        this.impaye = impaye;
    }

     public int getSolde() {
        return solde;
    }

    public void setSolde(int solde) {
        this.solde = montantcfu+impaye;
    }

    
    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

   
    
    
}
