/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu.data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import mtnz.util.logging.MLogger;
import mtngc.cfu.BundleSession;
import mtngc.cfu.Contribuable;

/**
 *
 * @author Administrateur
 */
public class DBSrverClient {
    
//      protected String Servername = "scanvascluster" , dbPort = "1521" , Databasename = "dbsrver ",dbPass = "app_user" , dbUser = "app_user" ;
      protected String dbIP = "10.4.0.160" , dbPort = "1521" , dbName = "dbsrver" , dbPass = "app_user" , dbUser = "app_user";
      
    

    
    
    public int update(String transactionId, String momoStatus, String momoMessage) throws Exception{
     int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("UPDATE APP_USER.CFU_TRANSACTION   ");
            sb.append("SET    MOMO_RESPONSE_DATE = sysdate, ");
            sb.append("MOMO_STATUS        = ? , ");
            sb.append("MOMO_MESSAGE       = ?  ");
            sb.append("Where TRANSACTION_ID     = ? ");
            
            
            
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, momoStatus);
            pstmt.setString(2, momoMessage);
            pstmt.setString(3, transactionId);
            
                        
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
                  

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
    public int insert (String transactionId,int msisdn, String idguincode, String nom,String prenom,String commune,String Quartier, int montant,int solde ) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO CFU_TRANSACTION ( ");
            sb.append("   TRANSACTION_ID, MSISDN, ");
            sb.append("   ID_GUINCODE, NOM, ");
            sb.append("   PRENOM, COMMUNE, QUARTIER,");
            sb.append("   MONTANT_PAYE, SOLDE ) ");
            sb.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
            
           
            
            
            
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, transactionId);
            pstmt.setInt(2, msisdn);
            pstmt.setString(3, idguincode);
            pstmt.setString(4, nom);
            pstmt.setString(5, prenom);
            pstmt.setString(6, commune);
            pstmt.setString(7, Quartier);
            pstmt.setInt(8, montant);
            pstmt.setInt(9, solde);
      
            
            
            
                        
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
                  

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  
    
 
    
     /*
     Determiner les Contribuable 
    */
     public Contribuable getInfoContribuable(String guincode){
        Contribuable InfoContribuable = null;
       
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
//SELECT * FROM APP_USER.CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='P952129';
            sb.append("SELECT * FROM APP_USER.CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='"+guincode+"'"); 
            
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {
                InfoContribuable  = new Contribuable();
                
                InfoContribuable.setId_guincode(guincode);
                String nom = rs.getString("NOM");
                InfoContribuable.setNom(nom);
                String  prenom = rs.getString("PRENOM");
                InfoContribuable.setPrenom(prenom);
                String commune = rs.getString("COMMUNE");
                InfoContribuable.setCommune(commune);
                String quartier =rs.getString("QUARTIER");
                InfoContribuable.setQuartier(quartier);  
                int montantcfu = rs.getInt("MONTANTCFU");
                InfoContribuable.setMontantcfu(montantcfu);
                int montantImpaye =rs.getInt("MONTANTCFU_IMPAYE");
                InfoContribuable.setImpaye(montantImpaye);
                        
                
                
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return InfoContribuable;
    }
     
      /*
     Determiner les montants encorurs 
    
    */
     public Contribuable getMontantEncours(String guincode){
        Contribuable MontantEncours = null;
       
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
    //SELECT montantcfu FROM APP_USER.CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='P952129';
            sb.append("SELECT montantcfu FROM CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='"+guincode+"'"); 
            
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {
                MontantEncours  = new Contribuable();
                
                MontantEncours.setId_guincode(guincode);              
                int montantcfu = rs.getInt("MONTANTCFU");
                MontantEncours.setMontantcfu(montantcfu);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return MontantEncours;
    }
     
       /*
     Determiner les montants impayes 
    
    */
     public Contribuable getImpayeMontant(String guincode){
        Contribuable impayeMontant = null;
       
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
    //SELECT montantcfu FROM APP_USER.CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='P952129';
            sb.append("SELECT montantcfu_impaye FROM APP_USER.CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='"+guincode+"'"); 
            
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {
                impayeMontant  = new Contribuable();
                
                impayeMontant.setId_guincode(guincode);              
                int impaye = rs.getInt("MONTANTCFU_IMPAYE");
                impayeMontant.setImpaye(impaye);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return impayeMontant;
    }
     
    //SELECT * FROM APP_USER.CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='P952129';
     public Contribuable getEligibleGuinCode(String guincode) {
        Contribuable resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT * FROM APP_USER.CFU_CONTRIBUABLE_PAYEMENT WHERE ID_GUINCODE ='"+guincode+"'"); 
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {

                String idguincode = rs.getString("ID_GUINCODE");

                resp = new Contribuable();
                resp.setId_guincode(idguincode);           
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }

    
  protected Connection getConnection() throws SQLException,ClassNotFoundException,InstantiationException,IllegalAccessException {

        Class.forName("oracle.jdbc.driver.OracleDriver");
        //jdbc:oracle:thin:@orap-cluster.mydomain.com:1521:myrac
        //oracle.jdbc.driver.OracleDriver
        //String driverName = "oracle.jdbc.driver.OracleDriver";
        //Class.forName(driverName).newInstance();
//        String serverIP = this.Servername;
//        String portNumber = this.dbPort;
//        String sid = this.Databasename;
//        String url = "jdbc:oracle:thin:@" + serverIP + ":" + portNumber + "/" + sid;
//        String username = this.dbUser;
//        String password = this.dbPass;
//        Connection conn = DriverManager.getConnection(url, username, password);
        String serverIP = this.dbIP;
        String portNumber = this.dbPort;
        String sid = this.dbName;
        String url = "jdbc:oracle:thin:@" + serverIP + ":" + portNumber + ":" + sid;
        String username = this.dbUser;
        String password = this.dbPass;
        Connection conn = DriverManager.getConnection(url, username, password);

        return conn;
    }
    
    
}