/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu.momo;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/**
 *
 * @author Administrateur
 */
public class SAXHandler extends DefaultHandler {
    private boolean isStatusCodeNode = false;
    private boolean isStatusDescNode = false;
    private boolean isProcessingNumberNode;
    
    private MoMoResponse response;
    private MoMoResponse tmp;
    
    MoMoResponse getMoMoResponse(){
        return response;
    }
    @SuppressWarnings("ConvertToStringSwitch")
    public void startElement(String uri, String localName,String qName, 
             Attributes attributes) throws SAXException {

        //System.out.println("Start Element :" + qName);

        if (qName.equals("ns3:StatusCode")) {
            isStatusCodeNode = true;
            
        }else if (qName.equals("ns3:StatusDesc")) {
            isStatusDescNode = true;
           
        }else if (qName.equals("ns3:ProcessingNumber")) {
            isProcessingNumberNode = true;
            tmp = new MoMoResponse();
        }
        
        
    }

    public void characters(char ch[], int start, int length) throws SAXException {

        if (isStatusCodeNode) {
            tmp.setStatusCode( new String(ch, start, length));
            //System.out.println("Full Name : " + new String(ch, start, length));
            //fullName = false;
            isStatusCodeNode = false;
        }else if (isStatusDescNode) {
            tmp.setStatusDesc( new String(ch, start, length));
            //System.out.println("Full Name : " + new String(ch, start, length));
            //fullName = false;
            isStatusDescNode = false;
            this.response = tmp;
        }else if (isProcessingNumberNode) {
            tmp.setTransactionId(new String(ch, start, length));
            //System.out.println("Full Name : " + new String(ch, start, length));
            //fullName = false;
            isProcessingNumberNode = false;
        }
    }
    
   
    
}
