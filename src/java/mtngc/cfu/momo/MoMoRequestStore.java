

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu.momo;

import java.io.Serializable;
import java.util.*;
import mtngc.cfu.BundleSession;
import mtngc.cfu.Contribuable;
import mtngc.cfu.Contribuable;
//
/**
 *
 * @author Administrateur
 */
public class MoMoRequestStore {
    private static MoMoRequestStore INSTANCE;
    private static final Object _lock = new Object();
    

    
    public static MoMoRequestStore getInstance() {
        if(INSTANCE == null){
            synchronized(_lock){
                if(INSTANCE == null){
                    INSTANCE = new MoMoRequestStore();
                }               
            }
        }
        return INSTANCE;
    }
    
    private HashMap request = new HashMap();
    
    private MoMoRequestStore() {
       
    }
    
    public void register(String transactionId, String msisdn,String ussdCode, BundleSession bundleSession){
        MoMoRequest oldReq = fetch(transactionId);
        if(oldReq != null){
            this.request.remove(transactionId);
        }
        
        MoMoRequest req = new MoMoRequest();        
        req.setMsisdn(msisdn);
        req.setTransactionId(transactionId);
        req.setIdguicode(bundleSession.getIdguicode());
        req.setMontant(bundleSession.getMontant());
        req.setUSSDCode(ussdCode);
        
        
        this.request.put(transactionId, req);
        
    }
    
    public MoMoRequest fetch(String transactionId){
        Object obj = this.request.get(transactionId);
        if (obj != null){
            return (MoMoRequest) obj;
        }else 
            return null;
    }
    
    public void remove(String transactionId){
        this.request.remove(transactionId);
    }
    

}
