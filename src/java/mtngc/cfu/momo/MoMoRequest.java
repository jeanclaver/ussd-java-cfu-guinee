/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu.momo;


import java.io.Serializable;
import mtngc.cfu.Contribuable;
import mtngc.cfu.data.DBSrverClient;

/**
 *
 * @author Administrateur
 */
    public class MoMoRequest implements Serializable {
        private String msisdn;
        private String transactionId;
        private String idguicode;
        private String USSDCode;
        private int montant; 

        /**
         * @return the msisdn
         */
        public String getMsisdn() {
            return msisdn;
        }

        /**
         * @param msisdn the msisdn to set
         */
        public void setMsisdn(String msisdn) {
            this.msisdn = msisdn;
        }

        /**
         * @return the transactionId
         */
        public String getTransactionId() {
            return transactionId;
        }

        /**
         * @param transactionId the transactionId to set
         */
        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

    /**
     * @return the USSDCode
     */
    public String getUSSDCode() {
        return USSDCode;
    }

    /**
     * @param USSDCode the USSDCode to set
     */
    public void setUSSDCode(String USSDCode) {
        this.USSDCode = USSDCode;
    }

    /**
     * @return the invoice
     */
    public String getIdguicode() {
        return idguicode;
    }

    /**
     * @param invoice the invoice to set
     */
    public void setIdguicode(String idguicode) {
        this.idguicode = idguicode;
    }

   

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

   
   
}
