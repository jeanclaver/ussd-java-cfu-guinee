/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu.momo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;


import mtngc.ussd.AbstractBundleSession;
import mtngc.ussd.AbstractMainMenu;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.ussd.AbstractUSSDHandler;
import mtngc.cfu.data.DBSrverClient;
//import mtngc.bundles.core.*;

import javax.xml.parsers.*;
import mtngc.cfu.BundleSession;
import mtngc.cfu.Contribuable;
import mtngc.cfu.USSDPush;
import org.xml.sax.*;
/**
 *
 * @author Administrateur
 */
public class MoMoPaymentHandler extends AbstractUSSDHandler {
    static String externalData1;
       
    public MoMoPaymentHandler(){
    }
    
        static
    {  
        ContextKeyName = "cfu";
        externalData1 = ContextKeyName;
        supportedMSISDNs = new ArrayList <Integer> ();
        String filePath = "C:\\Logs\\"+externalData1;
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
        
        //MLogger.setHomeDirectory("C:\\Logs\\"+ContextKeyName);
    }
        
    private String handleRequest(MoMoResponse momoResp,boolean provision){
        String str = null;
        MoMoRequestStore requestStore = MoMoRequestStore.getInstance();
        String transactionId = momoResp.getTransactionId();
        MoMoRequest momoReq = requestStore.fetch(transactionId);
        if(momoReq != null){
            String msisdn = momoReq.getMsisdn();
            String idguincode = momoReq.getIdguicode();
            int montant = momoReq.getMontant();
            String status = null;
            String momoMsg = null;
          
           if(provision){
                
                 MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|ID_GUINCODE:"+idguincode+"|MONTANT:"+montant+"|Transaction successfull on Mobile Money");
            
                status = "SUCCESS"; 
                requestStore.remove(transactionId);
            }else{
                status = "FAILED"; 
                 MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|ID_GUINCODE:"+idguincode+"|MONTANT:"+montant+"|Transaction NOT successfull on Mobile Money");
            
                momoMsg = momoResp.getStatusDesc();
                
                if(momoMsg.equals("TARGET_AUTHORIZATION_ERROR")){
                    str = "Solde insuffisant pour paiment "+idguincode+" GNF pur quittance " +montant;
                }else if(momoMsg.equals("ACCOUNT_NOT_FOUND")){
                    str = "Vous n'avez pas de compte MoMo. S'il vous plaît Veuillez contacter le service client au 111.";
                }else {
                    str = "Cette transaction ne peut etre effectuee, Veuillez contacter le service client au 111.";
                }
                
            }
           
            DBSrverClient db = new DBSrverClient();
           
            try{
                db.update(transactionId, status, momoMsg);
            }catch (Exception e){
                MLogger.Log(this, e);
            }
            
            MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|Sending USSD PUSH Message "+str);
                
            String ussdCode = momoReq.getUSSDCode();
            USSDPush push = new USSDPush();
            push.push(msisdn, str, ussdCode);

        }else {
            str = "Transaction successfull on Mobile Money but failed to find the original Mobile Money request. Can not be provisioned!!!!.";
            MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|"+str);
        }
        
        return str;
    }
    
    
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response){
        //String str1 = request.getQueryString();
        //Object obj = request.getParameterNames();
        TraceParametersAndHeaders(request);
        StringBuilder sb = new StringBuilder();
        try {
            MLogger.Log(this, LogLevel.DEBUG, "MoMo Response recieved. Fetching request body");
                    
            MoMoResponse momoResp =  getMoMoResponse(request);
            boolean mustProvision = false;
            if(momoResp != null){
                String statusCode = momoResp.getStatusCode();
                String transactionId = momoResp.getTransactionId();
                if(statusCode.equals("01")){
                    String str = this.handleRequest(momoResp, true);
                    sb.append(str);
                }else {
                    String str = this.handleRequest(momoResp, false);
                    sb.append(str);
                }           
            }else{
                String str = "Failed to process MoMo request.";
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, str);
            }
            
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", "FC");
            response.setHeader("cpRefId", "emk2545");
            
            PrintWriter out = response.getWriter();
//  
            out.append(sb.toString());
            MLogger.Log(this, LogLevel.ALL, sb.toString());
            out.close();
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
            
        
    }
    
    
    
    protected AbstractBundleSession CreateBundleSession(){
        return (AbstractBundleSession)new BundleSession();
    }
    protected AbstractMainMenu CreateMainMenu(String msisdn){
        return null;
    }
    
    private MoMoResponse getMoMoResponse(HttpServletRequest request)throws IOException,SAXException, ParserConfigurationException {
        
        String soapPayload = getBody(request);
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
	SAXParser parser = parserFactor.newSAXParser();
	SAXHandler handler = new SAXHandler();
    
        parser.parse(new ByteArrayInputStream(soapPayload.getBytes("utf-8")), handler);
        return handler.getMoMoResponse();
    }   
      
    private  String getBody(HttpServletRequest request) throws IOException {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }
     
    
    
}
