/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu;
import mtngc.ussd.AbstractBundleSession;
//import dbaccess.dbsrver.datareseller.DataPackage;

/**
 *
 * @author Administrateur
 */
public class BundleSession  extends AbstractBundleSession {
    private String ussdSessionId;
    private String ussdCode;
    private String idguicode;
    private int step;
    private int montant;   
    
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    public String getUssdCode() {
        return ussdCode;
    }

    public void setUssdCode(String ussdCode) {
        this.ussdCode = ussdCode;
    }

    public String getIdguicode() {
        return idguicode;
    }

    public void setIdguicode(String idguicode) {
        this.idguicode = idguicode;
    }

    

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

   

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }    
    
}

