/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu;
//import mtngc.bundles.core.*;

import mtngc.cfu.momo.MoMoRequestStore;
import java.util.Date;
import java.util.concurrent.TimeUnit;
//import dbaccess.dbsrver.*;

import ecwclient.ECWChannelEnum;
import ecwclient.ECWEngine;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
//import mdpclient.*;
//import mtngc.bundles.core.UserOption;
//import mtngc.sms.SMSClient;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import mtngc.cfu.data.DBSrverClient;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

/**
 *
 * @author Administrateur
 */
public class ProvisioningEngine {
private int optionNumber = 0;
private int Solde;
private int montant;
String FreeFlow;
 
 public String executeMoMoPayment(String msisdn, String transactionId, String ussdCode,BundleSession bundleSession){
    MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+transactionId+"|Guincode "+bundleSession.getIdguicode()+" Initiating MoMo Payment.");
    int msisdnInt = Integer.parseInt(msisdn);
    DBSrverClient dbClient = new DBSrverClient();
    String msg = null;
    Contribuable contribuable = dbClient.getInfoContribuable(bundleSession.getIdguicode());
    Contribuable montantEncours = dbClient.getMontantEncours(bundleSession.getIdguicode());
    Contribuable impaye = dbClient.getImpayeMontant(bundleSession.getIdguicode());
    Solde = montantEncours.getMontantcfu()+impaye.getImpaye();
    
       
            
        //transactionId = "201807141028070";        
           String str ="Mobile Money demande de paiement soumise avec succes";
           MoMoRequestStore requestStore = MoMoRequestStore.getInstance();
            requestStore.register(transactionId, msisdn, ussdCode,bundleSession);
            
            ECWEngine eng = new ECWEngine();
            msisdn = "224"+msisdn;// "224664222412";
            //int amount = 100;
            if(ConfirmMenu.PAYERMONTANTCFU == 1){
                 // Paiement de montant CFU
                montant = montantEncours.getMontantcfu();
                bundleSession.setMontant(montant);
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+transactionId+"|Selected Amount "+bundleSession.getMontant()+" Initiating MoMo Payment.");
            }else if(ConfirmMenu.IMPAYEMONTANTCFU == 2){
                  // Paiement de quittance impayé
                montant = impaye.getImpaye();
                bundleSession.setMontant(montant);
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+transactionId+"|Selected Amount "+bundleSession.getMontant()+" Initiating MoMo Payment.");
            }else if (ConfirmMenu.SOLDEMONTANTCFU == 3){
                 // Paiement de Solde Quittance du Contribuable
            montant = Solde;
            bundleSession.setMontant(montant);
            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+transactionId+"|Selected Amount "+bundleSession.getMontant()+" Initiating MoMo Payment.");
                 
             }
            eng.execute(msisdn, transactionId, bundleSession.getMontant(), bundleSession.getIdguicode(), ECWChannelEnum.TAX,false);
             try{
             MLogger.Log(this, LogLevel.DEBUG, "Session "+transactionId+"|Subscriber "+msisdnInt+"|Guincode:"+bundleSession.getIdguicode()+" |Nom:" +contribuable.getNom()+"| Prenom:"+contribuable.getPrenom()+"|Commune:"+contribuable.getCommune()+"|Quartier:"+contribuable.getQuartier()+"|Montant:"+bundleSession.getMontant()+"|Solde:"+Solde);  
             this.FreeFlow = "FB";

//            dbClient.insert(transactionId, msisdnInt, bundleSession.getIdguicode(), contribuable.getNom(),contribuable.getPrenom(),contribuable.getCommune(),contribuable.getQuartier(),bundleSession.getMontant(),Solde);
            }catch (Exception e){
                MLogger.Log(this, e);
            }
            return str; 
      
    }
    
    
    
    public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }
    
    
    
}
