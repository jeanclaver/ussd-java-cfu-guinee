/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.cfu;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;
import mtngc.cfu.data.DBSrverClient;
import mtngc.sms.SMSClient;
import mtngc.ussd.AbstractBundleSession;
import mtngc.ussd.AbstractMainMenu;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.ussd.AbstractUSSDHandler;
//import mtngc.bundles.core.UserOption;
//import mtngc.bundles.core.ProvisioningEngine;
import org.apache.commons.lang.StringUtils;
/**
 *
 * @author Administrateur
 */
public class RequestHandler extends AbstractUSSDHandler {
    
    static String externalData1;
    
    String FreeFlow;
    
    public RequestHandler(){
        
    }
    
    static
    {  
        ContextKeyName = "cfu";
        externalData1 = ContextKeyName;
        supportedMSISDNs = new ArrayList <Integer> ();
        String filePath = "C:\\Logs\\"+externalData1;
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
        
        //MLogger.setHomeDirectory("C:\\Logs\\"+ContextKeyName);
    }
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response){
            String msisdnStr = request.getParameter("MSISDN");
            //msisdnStr = "224660629885";
            try{msisdnStr = msisdnStr.substring(3);}catch(Exception n){};
            //
            
            handleRequest(msisdnStr,request,response);
    }  
 
    public void handleRequest(String msisdnStr, HttpServletRequest request, HttpServletResponse response){
        TraceParametersAndHeaders(request);
            
        try {
           
            int msisdn = 0;
            try{msisdn = Integer.parseInt(msisdnStr);}catch(Exception n){};
            
            if(IsMSISDNAllowed(msisdn)){
                processRequest(msisdnStr,request, response);
            }else {
                response.setContentType("UTF-8");
                response.setHeader("Freeflow", "FB");
                response.setHeader("cpRefId", "emk2545");
                PrintWriter out = response.getWriter();
                out.append("Le service n'est pas disponible");           
                out.close();            
            }          

        
        } catch (Exception e) {
            MLogger.Log(this, e);
        }    
    }
  
    
    private void processRequest(String msisdn, HttpServletRequest request, HttpServletResponse response){
        this.FreeFlow = "FC";
        try {
 
            String ussdSessionid = request.getParameter("SESSIONID");
            String newrequest = request.getParameter("newrequest");
            String input = request.getParameter("INPUT");
            
            ServletContext context = request.getServletContext();
              
            StringBuilder sb = new StringBuilder();
               
            MLogger.Log(this, LogLevel.DEBUG, "MSISDN:"+msisdn+"|Session "+ussdSessionid+"| Processing request.");

            String contextKey = GetContextKey(ussdSessionid);
            Object obj = context.getAttribute(contextKey);
            if(obj == null){
                if(input != null){
                    String ussdCode = input.trim();
                    String str = displayInvoiceMenu(context, ussdSessionid, msisdn,ussdCode);
                    sb.append(str );
                }else
                    sb.append("Application is running");
            }else {

                BundleSession bundleSession = (BundleSession) obj;
                int step = bundleSession.getStep();
                String ussdCode = bundleSession.getUssdCode();
                switch (step) {
                    case 1:
                        {
                            // if previous screen was the invoice menu
                            String str = processInvoiceNumberInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str );
                            break;
                        }
                    case 2: 
                        {
                            // if previous screen was the data bundle menu (bundle name) menu
                            String str = processPaymentAmountInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str );
                            break;
                        }
                    case 3:
                        {
                            // if previous screen was the data bundle menu (bundle name) menu
                            String str = processConfirmationInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str );
                            break;
                        }
                    default:
                        break;
                }

            }
            
            
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", this.FreeFlow);
            response.setHeader("cpRefId", "emk2545");
            
            PrintWriter out = response.getWriter();
//  
            out.append(sb.toString());
            MLogger.Log(this, LogLevel.ALL, sb.toString());
            out.close();
        
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
            
        
    }
    protected String displayInvoiceMenu(ServletContext context, String ussdSessionid, String msisdn, String ussdCode){
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Invoice Menu");
        BundleSession bundleSession = (BundleSession)CreateBundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        
        if(obj != null){
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);
        bundleSession.setUssdCode(ussdCode);
        bundleSession.setStep(1);

        context.setAttribute(contextKey, bundleSession);
        Menu menu = new Menu();
        String str = menu.getString();
        
        
        return str;
    }
    
    public String processInvoiceNumberInput(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
        
        String ussdCode = bundleSession.getUssdCode();
        if((input != null) && (input.matches("^[A-Za-z0-9]+$+"))  ){
                      
            input =  input.trim();
            if(input.equals("1")){
            bundleSession.setIdguicode(input);
            bundleSession.setStep(2);
            String contextKey = GetContextKey(ussdSessionid);
            context.setAttribute(contextKey, bundleSession);   
            AmountMenu amountMenu = new AmountMenu();
            msg = amountMenu.getString();
                
            }else {
            Menu menu = new Menu();
            msg = menu.getString(); 
            }
            

        }else{
            msg = this.displayInvoiceMenu(context, ussdSessionid, msisdn, ussdCode);
        }    
        
        return msg;
    }
    
    public String processPaymentAmountInput(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
        
        String ussdCode = bundleSession.getUssdCode();
        if((input != null) && ( !input.trim().equals("")) &&  (input.matches("^[A-Za-z0-9]+$+")) ){
                  
            input =  input.trim();
            if (input != null){
                bundleSession.setIdguicode(input);
                bundleSession.setStep(3);
                String contextKey = GetContextKey(ussdSessionid);
                context.setAttribute(contextKey, bundleSession);   
                ConfirmMenu confirmMenu = new ConfirmMenu();
                msg = confirmMenu.getString(bundleSession);
            }else{
                msg = this.processInvoiceNumberInput(input, msisdn, ussdSessionid, bundleSession, context);
            }
        }else{
            msg = this.processInvoiceNumberInput(input, msisdn, ussdSessionid, bundleSession, context);
        }    
        
        return msg;
    }

    public String processConfirmationInput(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
       
        String ussdCode = bundleSession.getUssdCode();
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim())) ){
           ProvisioningEngine provEngine = new ProvisioningEngine();
           msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, ussdCode, bundleSession);
           this.FreeFlow = "FB";
           this.Cleanup(context, ussdSessionid);
        }else{
            msg = this.displayInvoiceMenu(context, ussdSessionid, msisdn, ussdCode);
        }    
        
        return msg;
    }
    
    
    protected AbstractBundleSession CreateBundleSession(){
        return (AbstractBundleSession)new BundleSession();
    }
    protected AbstractMainMenu CreateMainMenu(String msisdn){
        //return (AbstractMainMenu) new PackageTypeMenu(msisdn);
        return null;
    } 

    private boolean isMsisdn(String input){
        boolean ok = false;
        if((input != null) && ( !input.trim().equals(""))){
            input = input.trim();
            //664222545
            if(input.length() == 9){
                if(StringUtils.isNumeric(input) ){
                    ok = true;
                }
            }
        }
        return ok;
    }
    
    private void SmsNotifcation(String msisdn){
        String msisdnStr = msisdn;
        msisdnStr = "224" + msisdnStr;// "224664222412";
        String msg = "CFU Message Guinee.";
        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("CFU", msisdnStr, msg);
        
    }
    private boolean Eligible(String guincode){
        boolean resp = false;
        DBSrverClient dbClient = new DBSrverClient();
        Contribuable dbBs = dbClient.getEligibleGuinCode(guincode);
        if(dbBs != null)
            resp = true;
        return resp;
    }
}
